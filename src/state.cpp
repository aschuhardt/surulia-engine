#include "state.hpp"

#include <cmath>
#include <iostream>
#include <limits>

#include "SDL.h"
#include "SDL_image.h"
#include "engine.hpp"

namespace surulia {

void State::base_update(double seconds) {
  std::lock_guard<std::mutex> lock(this->state_lock);
  this->on_update(seconds);
}

void State::base_draw(double seconds) {
  std::lock_guard<std::mutex> lock(this->state_lock);
  this->on_draw(seconds);
}

void State::base_mouse_moved(int x, int y, int dx, int dy) {
  std::lock_guard<std::mutex> lock(this->state_lock);
  this->on_mouse_moved(x, y, dx, dy);
}

void State::base_mouse_button(MouseButton button, ButtonAction action) {
  std::lock_guard<std::mutex> lock(this->state_lock);
  this->on_mouse_button(button, action);
}

void State::base_mouse_wheel(int x, int y) {
  std::lock_guard<std::mutex> lock(this->state_lock);
  this->on_mouse_wheel(x, y);
}

void State::base_keyboard(Key key, KeyAction action, KeyMod mod, bool repeat) {
  std::lock_guard<std::mutex> lock(this->state_lock);

  // handle deleting from text input buffer via backspace
  if (action == KeyAction::Pressed && key == Key::Backspace &&
      this->text_input_buffer != nullptr && !this->text_input_buffer->empty()) {
    std::shared_ptr<std::string>& buffer = this->text_input_buffer;

    // if holding CTRL, delete from last word boundary character to end
    // otherwise, delete last character
    auto rm_pos =
        mod == KeyMod::LCTRL || mod == KeyMod::RCTRL
            ? buffer->find_last_of(" ,.!?/\\\"\n'()-+=:;<>", std::string::npos)
            : buffer->size() - 1;

    // if holding CTRL and no word boundaries, delete from beginning
    if (rm_pos == std::string::npos) {
      rm_pos = 0;
    }

    buffer->erase(rm_pos, std::string::npos);
  }

  this->on_keyboard(key, action, mod, repeat);
}

void State::base_resize(int width, int height) {
  std::lock_guard<std::mutex> lock(this->state_lock);

  // upon resizing, some renderer resources have been-recreated; this requires
  // us to re-create our font
  for (int i = 0; i < this->window->fonts.size(); ++i) {
    this->window->fonts[i]->reload();
  }

  this->on_resize(width, height);
}

void State::base_load(Engine* engine) {
  this->engine = engine;
  this->window = this->engine->window.get();
  this->on_load();
}

void State::base_unload() {
  this->on_unload();
}

void State::base_text_input(const std::string& contents) {
  if (SDL_IsTextInputActive() == 0U)
    return;

  std::shared_ptr<std::string>& buffer = this->text_input_buffer;
  if (buffer != nullptr && buffer->size() < buffer->capacity()) {
    buffer->append(contents);
  }
}

void State::quit() {
  this->quit_requested.store(true);
}

std::weak_ptr<std::string> State::begin_text_input(int length) {
  if (this->text_input_buffer != nullptr) {
    this->text_input_buffer.reset();
  }

  if (SDL_IsTextInputActive() == 0U) {
    SDL_StartTextInput();
  }

  this->text_input_buffer = std::make_shared<std::string>();
  this->text_input_buffer->reserve(length);

  return this->text_input_buffer;
}

void State::end_text_input() {
  this->text_input_buffer.reset();

  if (SDL_IsTextInputActive() != 0U) {
    SDL_StopTextInput();
  }
}

void State::draw_rect(const Point& position,
                      const Size& dimensions,
                      const Color& color,
                      bool filled) const {
  auto renderer = static_cast<SDL_Renderer*>(this->engine->window->renderer);

  SDL_Rect rect = {position.x, position.y, dimensions.width, dimensions.height};
  SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, 255);
  if (filled) {
    SDL_RenderFillRect(renderer, &rect);
  } else {
    SDL_RenderDrawRect(renderer, &rect);
  }
}

void State::draw_line(const Point& start,
                      const Point& end,
                      const Color& color) const {
  auto renderer = static_cast<SDL_Renderer*>(this->engine->window->renderer);

  SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, 255);
  SDL_RenderDrawLine(renderer, start.x, start.y, end.x, end.y);
}

}  // namespace surulia
