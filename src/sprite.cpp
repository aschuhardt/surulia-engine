#include "sprite.hpp"

namespace surulia {

Sprite Sprite::empty(nullptr, {0, 0}, {0, 0});

void Sprite::draw(const Point& position, const Color& color) const {
  this->texture->draw(this->origin, this->bounds, position, this->bounds);
}

void Sprite::draw(const Point& position,
                  const Size& bounds,
                  const Color& color) const {
  this->texture->draw(this->origin, this->bounds, position, bounds);
}

const Sprite& Animation::get_frame() const {
  return this->frames.empty()
             ? Sprite::empty
             : this->frames.at(this->current_frame_index).source;
}

void Animation::update(double elapsed) {
  if (this->frames.empty() || this->frames.size() == 1) {
    return;
  }

  this->current_frame_progress += elapsed;

  if (this->current_frame_progress >=
      this->frames.at(this->current_frame_index).duration) {
    this->current_frame_progress = 0.0;
    this->current_frame_index++;

    if (static_cast<unsigned long>(this->current_frame_index) >
        this->frames.size() - 1) {
      this->current_frame_index = 0;
    }
  }
}

void Animation::add_frame(const Sprite& source, double duration) {
  this->frames.push_back({source, duration});
}

Size Animation::size() const {
  return this->get_frame().bounds;
}

}  // namespace surulia
