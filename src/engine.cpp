#include "engine.hpp"

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include <chrono>
#include <thread>

#include "log.hpp"
#include "state.hpp"

namespace surulia {

namespace {

constexpr double MAX_UPS(20.0);

}  // namespace

void Engine::start(const EngineConfig& config) {
  this->log->debug("Engine starting");

  if (!this->init_sdl()) {
    return;
  }

  // TODO: handle error case
  this->window = std::make_unique<Window>(config.width, config.height,
                                          config.window_title);

  // let the state->object perform its initialization step before beginning the
  // update routine
  state->base_load(this);

  // kick off the update routine on a background thread
  std::thread update_thread(Engine::update_routine, state.get(), this->log);

  bool quitting = false;
  auto last_frame_time = std::chrono::system_clock::now();
  auto renderer = this->window->renderer;
  while (!quitting && !state->quit_requested.load()) {
    this->poll_events(quitting);

    const Color& bg = this->clear_color;
    SDL_SetRenderDrawColor(renderer, bg.r, bg.g, bg.b, 255);
    SDL_RenderClear(renderer);

    // calculate the last frame's duration in fractional seconds
    const auto current_time = std::chrono::system_clock::now();
    std::chrono::duration<double> frame_duration =
        current_time - last_frame_time;

    // store the current timestamp for use on the next iteration
    last_frame_time = current_time;

    state->base_draw(frame_duration.count());

    SDL_RenderPresent(renderer);
  }

  this->log->debug("Waiting for update thread to terminate...");

  // kill the update thread
  state->parent_quitting.store(true);
  update_thread.join();

  // instruct state->to clean up textures etc.
  state->base_unload();

  this->cleanup_sdl();
}

bool Engine::init_sdl() {
  this->log->debug("Initializing SDL resources");

  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    this->log->error("Failed to initialize SDL video resources: {}",
                     SDL_GetError());
    return false;
  }

  if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
    this->log->error("Failed to initialize SDL_image: %s", IMG_GetError());
    return false;
  }

  if (TTF_Init() != 0) {
    this->log->error("Failed to initialize SDL_ttf: %s", TTF_GetError());
    return false;
  }

  return true;
}

void Engine::cleanup_sdl() {
  IMG_Quit();
  TTF_Quit();
  SDL_Quit();
}

void Engine::update_routine(State* state, std::shared_ptr<Log> log) {
  constexpr std::chrono::duration<double> minimum_duration(1.0 / MAX_UPS);

  log->debug("Beginning update loop");

  auto last_update_time = std::chrono::steady_clock::now();
  while (!state->quit_requested.load() && !state->parent_quitting.load()) {
    const auto current_time = std::chrono::steady_clock::now();
    std::chrono::duration<double> frame_duration =
        current_time - last_update_time;
    last_update_time = current_time;

    state->base_update(frame_duration.count());

    std::this_thread::sleep_until(current_time + minimum_duration);
  }

  log->debug("Update loop terminated");
}

void Engine::poll_events(bool& quit) {
  SDL_Event e;
  while (SDL_PollEvent(&e) != 0) {
    switch (e.type) {
      case SDL_QUIT:
        this->log->debug("Window closing");

        quit = true;
        return;
      case SDL_KEYDOWN: {
        this->log->debug("Key pressed");
        state->base_keyboard(
            Input::map_keycode(e.key.keysym.sym), KeyAction::Pressed,
            Input::map_keymod(e.key.keysym.mod), e.key.repeat != 0);
        break;
      }
      case SDL_KEYUP: {
        this->log->debug("Key released");
        state->base_keyboard(
            Input::map_keycode(e.key.keysym.sym), KeyAction::Released,
            Input::map_keymod(e.key.keysym.mod), e.key.repeat != 0);
        break;
      }
      case SDL_MOUSEMOTION: {
        const SDL_MouseMotionEvent mouse = e.motion;
        state->base_mouse_moved(mouse.x, mouse.y, mouse.xrel, mouse.yrel);
        break;
      }
      case SDL_MOUSEBUTTONUP:
        this->log->debug("Mouse button released");

        state->base_mouse_button(Input::map_mousebutton(e.button.button),
                                 ButtonAction::Released);
        break;
      case SDL_MOUSEBUTTONDOWN: {
        this->log->debug("Mouse button pressed");

        state->base_mouse_button(Input::map_mousebutton(e.button.button),
                                 ButtonAction::Pressed);
        break;
      }
      case SDL_MOUSEWHEEL: {
        const SDL_MouseWheelEvent wheel = e.wheel;
        state->base_mouse_wheel(wheel.x, wheel.y);
        break;
      }
      case SDL_WINDOWEVENT: {
        const SDL_WindowEvent window = e.window;
        if (window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
          this->log->debug("Window resized (%dx%d)", window.data1,
                           window.data2);

          state->base_resize(window.data1, window.data2);
        }
        break;
      }
      case SDL_TEXTINPUT: {
        state->base_text_input(e.text.text);
      } break;
      default:
        break;
    }
  }
}

}  // namespace surulia
