#include "wrappers/window.hpp"

#include <SDL.h>

#include "log.hpp"

namespace surulia {

Window::Window(int width, int height, const std::string& title) {
  Log log("Window");
  log.debug("Creating window with size %dx%d", width, height);

  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0");
  this->window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED, width, height,
                                  SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
  if (!this->window) {
    log.error("Failed to create window: %s", SDL_GetError());
    return;
  }

  this->renderer = SDL_CreateRenderer(
      this->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  if (!this->renderer) {
    log.error("Failed to create accelerated renderer: %s", SDL_GetError());
  }
}

Window::~Window() {
  // clean up owned resources *first* since their textures rely on the existence
  // of the SDL_Window and SDL_Renderer
  this->fonts.clear();
  this->textures.clear();

  if (this->renderer) {
    SDL_DestroyRenderer(this->renderer);
  }

  if (this->window) {
    SDL_DestroyWindow(this->window);
  }
}

}  // namespace surulia
