#include "wrappers/texture.hpp"

#include <SDL.h>
#include <SDL_image.h>

#include "log.hpp"
#include "wrappers/window.hpp"

namespace surulia {

Texture* Texture::load(Window* window, const std::string& path) {
  std::unique_ptr<Texture> texture(new Texture(window, path));
  window->textures.push_back(std::move(texture));
  return window->textures.back().get();
}

Texture::Texture(Window* window, const std::string& path) : window(window) {
  Log log("Texture");

  if (window == nullptr) {
    log.error("Can't load a texture an unitialized window");
    return;
  }

  auto surf = IMG_Load(path.c_str());

  if (surf == nullptr) {
    log.error("Failed to load texture file at \"%s\": %s", path.c_str(),
              IMG_GetError());
    return;
  }

  auto texture = SDL_CreateTextureFromSurface(window->renderer, surf);

  if (texture == nullptr) {
    log.error("Failed to convert surface (\"%s\") to native texture data: %s",
              path.c_str(), SDL_GetError());

    // don't need to return, we'll free the surface and return nullptr anyway
  } else {
    SDL_QueryTexture(texture, nullptr, nullptr, &this->bounds.width,
                     &this->bounds.height);
  }

  SDL_FreeSurface(surf);

  this->ptr = texture;
}

Texture::~Texture() {
  if (this->ptr != nullptr) {
    SDL_DestroyTexture(this->ptr);
  }
}

void Texture::draw(const Point& position, const Size& bounds) const {
  this->draw({0, 0}, bounds, position, bounds);
}

void Texture::draw(const Point& position) const {
  this->draw({0, 0}, this->bounds, position, this->bounds);
}

void Texture::draw(const Point& src_position,
                   const Size& src_bounds,
                   const Point& dest_position,
                   const Size& dest_bounds) const {
  const SDL_Rect src = {src_position.x, src_position.y, src_bounds.width,
                        src_bounds.height};
  const SDL_Rect dest = {dest_position.x, dest_position.y, dest_bounds.width,
                         dest_bounds.height};

  if (this->ptr != nullptr) {
    SDL_SetTextureColorMod(this->ptr, color_mod.r, color_mod.g, color_mod.b);
    SDL_RenderCopy(this->window->renderer, this->ptr, &src, &dest);
  }
}

}  // namespace surulia
