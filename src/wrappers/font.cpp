#include "wrappers/font.hpp"

#include <SDL_FontCache.h>

#include "wrappers/window.hpp"

namespace surulia {

namespace {

void load_font(FC_Font** ptr,
               SDL_Renderer* renderer,
               const std::string& path,
               int size) {
  FC_LoadFont(*ptr, renderer, path.c_str(), size, {255, 255, 255, 255},
              TTF_STYLE_NORMAL);
  FC_SetFilterMode(*ptr, FC_FILTER_NEAREST);
}

}  // namespace

Font::Font(Window* window, const std::string& path, int size)
    : ptr(FC_CreateFont()), window(window), file_path(path), size(size) {
  if (this->ptr) {
    load_font(&this->ptr, this->window->renderer, this->file_path, this->size);
  }
}

Font* Font::load(Window* window, const std::string& path, int size) {
  std::unique_ptr<Font> font(new Font(window, path, size));
  window->fonts.push_back(std::move(font));
  return window->fonts.back().get();
}

Font::~Font() {
  if (this->ptr != nullptr) {
    FC_FreeFont(this->ptr);
  }
}

void Font::reload() {
  if (this->ptr != nullptr) {
    FC_FreeFont(this->ptr);
  }

  this->ptr = FC_CreateFont();
  load_font(&this->ptr, this->window->renderer, this->file_path, this->size);
}

int Font::width(const std::string& contents) const {
  return FC_GetWidth(this->ptr, contents.c_str());
}

int Font::height() const {
  return FC_GetLineHeight(this->ptr);
}

void Font::draw(const Color& color,
                const Point& position,
                const std::string& contents) const {
  FC_DrawColor(this->ptr, this->window->renderer, position.x, position.y,
               {color.r, color.g, color.b, 255}, contents.c_str());
}

void Font::draw_bounded(const Color& color,
                        const Point& position,
                        const Size& bounds,
                        const std::string& contents) const {
  FC_DrawBoxColor(this->ptr, this->window->renderer,
                  {position.x, position.y, bounds.width, bounds.height},
                  {color.r, color.g, color.b, 255}, contents.c_str());
}

}  // namespace surulia
