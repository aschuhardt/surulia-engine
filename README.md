# Surulia 2D Game Engine

## Features

- Text input and rendering
- 2D primitives drawing
- Sprite animation sequencing
- Parallel update thread for game logic
- A convenient, object-oriented design

## Build instructions:

### All platforms

1. Install `SDL2`, `SDL2_Image`, and `SDL2_TTF` development libraries
2. Clone this repository and `cd` into it
3. `git submodule update --init --recursive`
4. `mkdir build && cd build`

### Linux

5. `cmake ..`
6. `make example && ./example`

### Windows (MSVC)

5. Launch Visual Studio developer command prompt and navigate to the `build` directory
6. `cmake -DSDL2_PATH="<path to your SDL2 installation>" -DSDL2_IMAGE_PATH="<path to your SDL2_Image installation>" -DSDL2_TTF_PATH="<path to your SDL2_TTF installation>"`
7. Run `msbuild example.vcxproj` to build the example
8. `./Debug/example.exe`