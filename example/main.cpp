#include <iostream>
#include <memory>
#include <numeric>
#include <sstream>
#include <string>

#include "engine.hpp"
#include "state.hpp"

using namespace surulia;

class GameState : public State {
 public:
  GameState() : text_input(begin_text_input(100)) {}
  virtual ~GameState() {}

 protected:
  void on_load() override {
    // load some resources from disk
    this->font = Font::load(this->window, "font.ttf", 16);
    this->particle_texture = Texture::load(this->window, "particle.png");

    // build an animation
    std::array<Point, 6> offsets{
        {{16, 0}, {32, 0}, {48, 0}, {64, 0}, {48, 0}, {32, 0}}};
    for (auto offset : offsets) {
      Sprite frame(this->particle_texture, offset, {16, 16});
      this->particle.add_frame(frame, 0.1);
    }
  }

  void on_draw(double seconds) override {
    // draw some text within pre-defined boundsi
    font->draw_bounded(
        DARK_CYAN, {300, 20}, {100, 100},
        "Here is some size-bounded text.  It should span multiple lines.");

    // update our animation and
    this->particle.update(seconds);
    this->particle.get_frame().draw({100, 100}, GREEN);

    if (auto text = this->text_input.lock()) {
      this->font->draw(DARK_YELLOW, {400, 400}, *text);
    }
  }

  void on_keyboard(Key key,
                   KeyAction action,
                   KeyMod mod,
                   bool repeat) override {
    if (key == Key::Escape && action == KeyAction::Pressed) {
      this->quit();
    }
  }

 private:
  // text entry buffer; for this example it will last the lifetime of the state
  // object.  in real-world implementations such a buffer would be created as
  // needed via State::begin_text_input.
  std::weak_ptr<std::string> text_input;

  // arranges a sequence of animation frames and updates an index according to
  // those frames' durations
  Animation particle;

  Texture* particle_texture = nullptr;
  Font* font = nullptr;
};

int main(void) {
  EngineConfig config;

  Engine engine(std::static_pointer_cast<State>(std::make_shared<GameState>()));

  engine.start(config);

  return 0;
}
