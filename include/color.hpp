#pragma once

namespace surulia {

struct Color {
  constexpr Color(unsigned char r, unsigned char g, unsigned char b)
      : r(r), g(g), b(b) {}

  constexpr bool operator==(const Color& other) const {
    return this->r == other.r && this->g == other.g && this->b == other.b;
  }

  constexpr bool operator!=(const Color& other) const {
    return !(operator==(other));
  }

  unsigned char r = 0, g = 0, b = 0;
};

constexpr Color BLACK(0, 0, 0);
constexpr Color WHITE(255, 255, 255);

constexpr Color RED(255, 0, 0);
constexpr Color YELLOW(255, 255, 0);
constexpr Color GREEN(0, 255, 0);
constexpr Color BLUE(0, 0, 255);
constexpr Color CYAN(0, 255, 255);
constexpr Color MAGENTA(255, 0, 255);

constexpr Color DARK_RED(127, 0, 0);
constexpr Color DARK_YELLOW(127, 127, 0);
constexpr Color DARK_GREEN(0, 127, 0);
constexpr Color DARK_BLUE(0, 0, 127);
constexpr Color DARK_CYAN(0, 127, 127);
constexpr Color DARK_MAGENTA(127, 0, 127);

}  // namespace surulia

