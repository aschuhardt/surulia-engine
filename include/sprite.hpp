#pragma once

#include <vector>

#include "color.hpp"
#include "utility.hpp"
#include "wrappers/texture.hpp"

namespace surulia {

class Sprite {
 public:
  Sprite(Texture* texture, const Point& origin, const Size& bounds)
      : texture(texture), origin(origin), bounds(bounds) {}
  void draw(const Point& position, const Color& color = WHITE) const;
  void draw(const Point& position,
            const Size& bounds,
            const Color& color = WHITE) const;

  static Sprite empty;

  Size bounds;
  Point origin;

 private:
  Texture* texture;
  // double scale = 1.0;
};

struct KeyFrame {
  Sprite source = Sprite::empty;
  double duration = 0.0;
};

class Animation {
 public:
  const Sprite& get_frame() const;
  void update(double elapsed);
  void add_frame(const Sprite& source, double duration);
  Size size() const;

 private:
  std::vector<KeyFrame> frames;
  double current_frame_progress = 0.0;
  int current_frame_index = 0;
};

}  // namespace surulia
