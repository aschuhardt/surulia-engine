#pragma once

namespace surulia {

struct Size {
  int width = 0, height = 0;
};

struct Point {
  int x = 0, y = 0;
};

}  // namespace surulia
