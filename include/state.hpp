#pragma once

#include <atomic>
#include <memory>
#include <mutex>
#include <string>

#include "color.hpp"
#include "input.hpp"
#include "log.hpp"
#include "sprite.hpp"
#include "utility.hpp"
#include "wrappers/window.hpp"

namespace surulia {

class Engine;

class State {
 public:
  State() : log("State") {}
  void base_update(double seconds);
  void base_draw(double seconds);
  void base_mouse_moved(int x, int y, int dx, int dy);
  void base_mouse_button(MouseButton button, ButtonAction action);
  void base_mouse_wheel(int x, int y);
  void base_keyboard(Key key, KeyAction action, KeyMod mod, bool repeat);
  void base_resize(int width, int height);
  void base_load(Engine* engine);
  void base_text_input(const std::string& contents);
  void base_unload();

  std::weak_ptr<std::string> begin_text_input(int length);
  void end_text_input();
  void draw_rect(const Point& position,
                 const Size& dimensions,
                 const Color& color,
                 bool filled) const;
  void draw_line(const Point& start,
                 const Point& end,
                 const Color& color) const;

  std::atomic_bool parent_quitting = ATOMIC_VAR_INIT(false);
  std::atomic_bool quit_requested = ATOMIC_VAR_INIT(false);

 private:
  std::mutex state_lock;
  std::shared_ptr<std::string> text_input_buffer = nullptr;
  Engine* engine = nullptr;

 protected:
  void quit();
  virtual void on_draw(double seconds) {}
  virtual void on_update(double seconds) {}
  virtual void on_mouse_moved(int x, int y, int dx, int dy) {}
  virtual void on_mouse_button(MouseButton button, ButtonAction action) {}
  virtual void on_mouse_wheel(int x, int y) {}
  virtual void on_keyboard(Key key, KeyAction action, KeyMod mod, bool repeat) {
  }
  virtual void on_resize(int width, int height) {}
  virtual void on_load() {}
  virtual void on_unload() {}

  Window* window = nullptr;
  Log log;
};

}  // namespace surulia
