#pragma once

#include <array>
#include <iostream>
#include <mutex>

#include "../lib/fmt/include/fmt/format.h"

#define LOG_LEVEL_NAME(level) (#level)

namespace surulia {

enum class LogLevel { Info = 0, Error, Warn, Debug };

class Log {
 public:
  Log(const std::string& name) : name(name) {}

  void set_level(LogLevel level) {
    std::lock_guard<std::mutex> lock(this->lock);
    this->level = level;
  }

  template <typename... Args>
  void info(const std::string& fmt, Args... args) {
    this->write(LogLevel::Info, fmt, args...);
  }

  template <typename... Args>
  void error(const std::string& fmt, Args... args) {
    this->write(LogLevel::Error, fmt, args...);
  }

  template <typename... Args>
  void warn(const std::string& fmt, Args... args) {
    this->write(LogLevel::Warn, fmt, args...);
  }

  template <typename... Args>
  void debug(const std::string& fmt, Args... args) {
    this->write(LogLevel::Debug, fmt, args...);
  }

 protected:
  template <typename L, typename... Args>
  void write(L level, const std::string& fmt, Args... args) {
    if (level > this->level) return;
    std::lock_guard<std::mutex> lock(this->lock);
    std::cout << "[" << LOG_LEVEL_NAME(L) << "]"
              << "[" << this->name << "] " << fmt::format(fmt, args...) << std::endl;
  }

 private:
  std::array<char, 1024> buffer;
  LogLevel level = LogLevel::Info;
  std::mutex lock;
  std::string name;
};

}  // namespace surulia
