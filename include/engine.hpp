#pragma once

#include <memory>
#include <string>
#include <type_traits>

#include "color.hpp"
#include "log.hpp"
#include "wrappers/window.hpp"

namespace surulia {

class State;

struct EngineConfig {
  int width = 800, height = 600;
  std::string window_title = "Surulia Engine";
};

class Engine {
 public:
  Engine(std::shared_ptr<State> state) : state(state) {}

  void start(const EngineConfig& config);

  Color clear_color = BLACK;
  LogLevel log_level = LogLevel::Info;
  std::shared_ptr<Log> log = std::make_shared<Log>("Engine");
  std::unique_ptr<Window> window = nullptr;

 private:
  bool init_sdl();
  static void cleanup_sdl();
  void poll_events(bool& quit);
  static void update_routine(State* state, std::shared_ptr<Log> log);

  std::shared_ptr<State> state;
};

}  // namespace surulia
