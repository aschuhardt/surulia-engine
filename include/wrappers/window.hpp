#pragma once

#include <memory>
#include <string>
#include <vector>

#include "wrappers/font.hpp"
#include "wrappers/texture.hpp"

struct SDL_Renderer;
struct SDL_Window;

namespace surulia {

class Window {
 public:
  Window(int width, int height, const std::string& title);
  Window(const Window& other) = delete;
  Window& operator=(const Window& other) = delete;
  ~Window();

  SDL_Renderer* renderer = nullptr;
  SDL_Window* window = nullptr;

  // window owns its related resources to ensure that they are deleted first
  std::vector<std::unique_ptr<Font>> fonts;
  std::vector<std::unique_ptr<Texture>> textures;
};

}  // namespace surulia
