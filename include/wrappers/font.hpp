#pragma once

#include <memory>
#include <string>

#include "color.hpp"
#include "utility.hpp"

struct FC_Font;

namespace surulia {

class Window;

struct Font {
 public:
  Font(const Font& other) = delete;
  Font& operator=(const Font& other) = delete;
  Font& operator=(Font&& other) = delete;
  ~Font();

  static Font* load(Window* window, const std::string& path, int size);

  int width(const std::string& contents) const;
  int height() const;
  void draw(const Color& color,
            const Point& position,
            const std::string& contents) const;
  void draw_bounded(const Color& color,
                    const Point& position,
                    const Size& bounds,
                    const std::string& contents) const;
  void reload();

 private:
  Font(Window* window, const std::string& path, int size);
  Window* window = nullptr;
  FC_Font* ptr = nullptr;
  std::string file_path;
  int size;
};

}  // namespace surulia
