#pragma once

#include <memory>
#include <string>

#include "color.hpp"
#include "utility.hpp"

struct SDL_Texture;

namespace surulia {

class Window;

class Texture {
 public:
  Texture(const Texture& other) = delete;
  Texture& operator=(const Texture& other) = delete;
  Texture& operator=(Texture&& other) = delete;
  ~Texture();

  static Texture* load(Window* window, const std::string& path);

  void draw(const Point& position) const;
  void draw(const Point& position, const Size& bounds) const;
  void draw(const Point& src_position,
            const Size& src_bounds,
            const Point& dest_position,
            const Size& dest_bounds) const;

  Color color_mod = WHITE;
  Size bounds = {0, 0};

 private:
  Texture(Window* window, const std::string& path);
  SDL_Texture* ptr = nullptr;
  Window* window = nullptr;
};

};  // namespace surulia